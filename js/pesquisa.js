  pacContainerInitialized = false;
   
   var origem = null;
   var destino = null;
      
   function pesquisaRota(origem_cidade,origem_uf,destino_cidade,destino_uf) {

   	var dados = {
                  "acao": "consulta",
                  "origem_cidade": origem_cidade,
                  "origem_uf": origem_uf,
                  "destino_cidade": destino_cidade,
                  "destino_uf": destino_uf
            };
            
   $.ajax({
	    url : 'rota.php',
	    type : 'post',
	    dataType: 'json',
	    data : dados,
		success: function(x){
			
			if (x.sucesso === 1) {
                           var html_linhas = '';
                           // insere os resultados na lista detro de resultado_buca
                           var total_linhas = x.linhas.length;
                           for (i = 0; i < total_linhas; i++) {
                              
                              html_linhas += " <li><a href=\"perfil.php?pid="+x.linhas[i].USUARIO_ID+"\" rel=\"external\">"+x.linhas[i].NOME_USUARIO+"</a></li>\n";
                              
                           }
                           
                           $('#lista_resultado_buca').html(html_linhas);
                           $('#lista_resultado_buca').listview('refresh');
                           
                           
                        } else {
                           alert('n�o cadastrou');
                        }
			
		},
		error : function(x) {
			alert('N�o foi poss�vel cadastrar este usu�rio.');
		}
    });
   
   
   }
   
    $(document).ready(function(){

     // ORIGEM
       $('#origem_criteria').keypress(function() {
               if (!pacContainerInitialized) {
                       $('.pac-container').css('z-index', '9999');
                       pacContainerInitialized = true;
               }
       });

       $("#origem_criteria").geocomplete()
          .bind("geocode:result", function(event, result){
            $("#label_btn_origem").html(result.formatted_address);
            
            origem = result;
            
            $("#popupOrigem").popup("close");
          })
          .bind("geocode:error", function(event, status){
            alert("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
            alert("Multiple: " + results.length + " results found");
          });


       $('#origem_buton').on('click',function(){

         $("#popupOrigem").popup({ history: false,
                 afteropen: function( event, ui ) {
                         $('#origem_criteria').focus();
                 }
         });

         $("#popupOrigem").popup("open");


       });


        // DESTINO
       $('#destino_criteria').keypress(function() {
               if (!pacContainerInitialized) {
                       $('.pac-container').css('z-index', '9999');
                       pacContainerInitialized = true;
               }
       });

       $("#destino_criteria").geocomplete()
          .bind("geocode:result", function(event, result){
            $("#label_btn_destino").html(result.formatted_address);
            
            destino = result;
            
            $("#popupDestino").popup("close");
          })
          .bind("geocode:error", function(event, status){
            alert("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
            alert("Multiple: " + results.length + " results found");
          });


       $('#destino_buton').on('click',function(){

         $("#popupDestino").popup({ history: false,
                 afteropen: function( event, ui ) {
                         $('#destino_criteria').focus();
                 }
         });

         $("#popupDestino").popup("open");


       });
       
       $('#pesquisa_pesquisar_btn').on('click',function(){
         
         // se origem e destino foram definidos
         if (origem && destino) {
            
            var positionOrigem = origem.geometry.location;
            var positionDestino= destino.geometry.location;
            
            var origem_cidade = origem.address_components[0].short_name;
            var origem_uf    = origem.address_components[1].short_name;
            
            var destino_cidade = destino.address_components[0].short_name;
            var destino_uf    = destino.address_components[1].short_name;
            
            pesquisaRota(origem_cidade,origem_uf,destino_cidade,destino_uf);
         }
         
       });
      
      



    });