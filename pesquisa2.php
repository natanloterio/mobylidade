<!DOCTYPE HTML>
<html lang="en-US">
    
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1"> 
 <title>Titulo da pagina</title> 

		
		<link rel="stylesheet" href="lib/jquery/jquery.mobile.custom.structure.css" />
		<link rel="stylesheet" href="lib/jquery/jquery.mobile.custom.theme.css" />
		<script src="lib/jquery/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
		<script src="lib/jquery/jquery.mobile-1.3.1.min.js"></script>
		<script src="pesquisa2/jquery.ui.autocomplete.min.js" type="text/javascript"></script>	
		
		<script type="text/javascript" src="pesquisa2/jquery.ui.map.full.min.js"></script>
		<script type="text/javascript" src="pesquisa2/jquery.ui.map.extensions.js"></script>

 <script>
//<![CDATA[	
			$(function() {
			
				function PlacesSearchControl(str) {
					var el = document.createElement('DIV');
					var wrap = document.createElement('DIV');
					var input = document.createElement('INPUT');
					el.appendChild(wrap);
					wrap.appendChild(input);
					$(el).attr('id', 'control');
					$(input).addClass('ui-bar-d ui-input-text ui-body-null ui-corner-all ui-shadow-inset ui-body-d ui-autocomplete-input');
					$(input).attr('id', 'places');
					$(input).val(str);
					$('#map_canvas').gmap('autocomplete', input, function(ui) {
						$('#map_canvas').gmap('clear', 'markers');
						$('#map_canvas').gmap('set', 'bounds', null);
						$.mobile.pageLoading();
						$('#map_canvas').gmap('placesSearch', { 'location': ui.item.position, 'radius': '5000'/*, 'name': ['store']*/ }, function(results, status) {
							if ( status === 'OK' ) {
								$.each(results, function(i, item) {
									$('#map_canvas').gmap('addMarker', { 'id': item.id, /*'icon': item.icon,*/ 'position': item.geometry.location, 'bounds':true }).click(function() {
										$('#map_canvas').gmap('openInfoWindow', {'content': '<h4>'+item.name+'</h4>'}, this);
									});
								});
							}
							$.mobile.pageLoading(true);
						});
					});
					$(input).focus(function() {
						if ( $(this).val() === str ) {
							$(this).val('');
						}
					}).blur(function() {
						if ( $(this).val() == '' ) {
							$(this).val(str);
						}
					});
					return el;
				}
				
				$('#map_canvas').gmap({'center': '58.00, 12.00', 'zoom': 10, 'disableDefaultUI': true, 'mapTypeId': 'terrain'}).bind('init', function(event, map) { 
					$('#map_canvas').gmap('addControl', new PlacesSearchControl('Search places...'), 1);
				});
				
			});
		//]]>	

</script>
 <script>

 </script>
</head>    

 <body>
<!-- Inicio da pagina -->
<div id="div_cadastrousuario" data-role="page">
	<!-- Menu lateral esquerda-->
	<div class="ui-responsive-panel">
		<div data-role="panel" data-display="push" id="menu_panel" data-theme="a">
			<!-- panel content goes here -->
			<a href="#my-header" data-rel="close">Close panel</a>
			<br>
			<br>
			<br>
			<ul data-role="listview" data-theme="a">
				<li><a href="#feed">Feed</a></li>
				<li><a href="audi.html">Audi</a></li>
				<li><a href="bmw.html">BMW</a></li>
			</ul>
		</div>
	</div>
	<!-- /panel -->
	<!-- Inicio cabecalho da pagina -->
	<div data-role="header">
		<a class="ui-icon-menu" href="#" data-role="button" data-icon="menu" data-theme="a">Menu</a>
		<h1>Nome da pagina</h1>
	</div>
	<!-- Fim cabecalho da pagina -->
	<!-- Inicio conteudo da pagina -->
	<div data-role="content" class="content">
		<a class="pesquisa_lugar_btn" id="origem_buton" href="#" data-role="button" data-icon="menu" data-theme="a">Origem</a>
		<a class="pesquisa_lugar_btn" id="destino_buton" href="#" data-role="button" data-icon="menu" data-theme="a">Destiono</a>
		<a href="#" id="login_botao" data-role="button" data-icon="check" data-theme="a">Pedir um guincho</a>

				


	</div>
	<!-- Fim conteudo da pagina -->
</div>
<!-- Fim da pagina-->
 <div data-role="popup" id="popupBasic">
  <div data-role="content" class="content">
   
     <div id="map_canvas" style="height:300px;"></div>
   
  </div>
 </div>
</body>

</html>