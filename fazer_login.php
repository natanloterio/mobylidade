<?php
		function getPass(){
		
			if(isset($_POST['lsenha'])){
				echo $_POST['lsenha'];
			}else{
				echo "";			
			}
		
		}
		
		function getUsername(){
		
			if(isset($_POST['lusername'])){
				echo $_POST['lusername'];
			}else{
				echo "";			
			}
		
		}

?>

<!DOCTYPE HTML>
<html lang="en-US">
    
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1"> 
 <title>Login</title> 

<?php require_once('includes-basicos.php');?> 
 
 <script src="js/fazer_login.js"></script>
 <script> 
  $(document).ready(function(){
	
	$('#login_botao').on('click',function(){
		
		usuario = $('#login').val();
		senha = $('#senha').val();
		
		logar(usuario,senha);
	});
	
	
	$('.ui-icon-menu').on('click',function(){
			
			$( "#menu_panel" ).panel( "open" );
	
	});
	
	
	
  
  });
</script>
</head>    

    <body>
  <!-- Inicio da pagina -->
	<div id="div_login" data-role="page" >
	
	<!-- Menu lateral esquerda-->
	<?php include('menu-lateral.php'); ?>
	<!-- /panel -->			
	  
	  
	  
	  <!-- Inicio cabecalho da pagina -->
		<div data-role="header"> 
			<a class="ui-icon-menu" href="#" data-role="button" data-icon="menu" data-theme="a">Menu</a>
		
			<h1>Login</h1>
		
		</div>
		<!-- Fim cabecalho da pagina -->
	  
		   <!-- Inicio conteudo da pagina -->  
        <div data-role="content"  class="content"> 
		   
		<label for="login">Usuario</label>
		<input type="text" id="usuario" value="<?php getUsername();?>">
		
		<label for="senha">Senha</label>
		<input type="password" id="senha" value="<?php getPass();?>">
		
		
		<a href="#" id="login_botao" data-role="button" data-icon="check" data-theme="a">Entrar</a>			
		
		<?php if(isset($_GET['login_error'])){ 
		echo "
		<label for=\"erro\">Erro login</label>	
		<input id=\"erro\"type=\"hidden\">";
		
		}?>
		
	    </div>
	   <!-- Fim conteudo da pagina -->  
	   
	   
	</div>
  <!-- Fim da pagina-->
 
    </body>

</html>